package com.prehax.award.models;

public class Award {
    private String yearMonth;
    private int award;

    public Award(String yearMonth, int award) {
        this.yearMonth = yearMonth;
        this.award = award;
    }

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }

    public int getAward() {
        return award;
    }

    public void setAward(int award) {
        this.award = award;
    }

    @Override
    public String toString() {
        return "Award{" +
                "yearMonth='" + yearMonth + '\'' +
                ", award=" + award +
                '}';
    }
}
