package com.prehax.award.models;

import com.prehax.award.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "transaction")
public class Transaction {
    @Id
    private long tid;
    @Column(name = "customer_cid")
    private int customerCid;
    private double amount;
    private int award;
    private Timestamp date;
    public Transaction() {}
    public Transaction(long tid, int customerCid, double amount, int award, Timestamp date) {
        this.tid = tid;
        this.customerCid = customerCid;
        this.amount = amount;
        this.award = award;
        this.date = date;
    }

    public long getTid() {
        return tid;
    }

    public void setTid(long tid) {
        this.tid = tid;
    }

    public int getCustomerCid() {
        return this.customerCid;
    }

    public void setCustomerCid(int customerCid) {
        this.customerCid = customerCid;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getAward() {
        return award;
    }

    public void setAward(int award) {
        this.award = award;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "tid=" + tid +
                ", customerCId=" + customerCid +
                ", amount=" + amount +
                ", award=" + award +
                ", date=" + date +
                '}';
    }
}
