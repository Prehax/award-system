package com.prehax.award.models;

import java.util.*;

public class CustomerAwards {
    private int cid;
    private String name;
    private int totalAward;
    private List<Award> awards;

    public CustomerAwards() { }

    public CustomerAwards(int cid, String name, int totalAward, List<Award> awards) {
        this.cid = cid;
        this.name = name;
        this.totalAward = totalAward;
        this.awards = awards;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalAward() {
        return totalAward;
    }

    public void setTotalAward(int totalAward) {
        this.totalAward = totalAward;
    }

    public List<Award> getAwards() {
        return awards;
    }

    public void setAwards(List<Award> awards) {
        this.awards = awards;
    }

    @Override
    public String toString() {
        return "CustomerAwards{" +
                "cid=" + cid +
                ", name='" + name + '\'' +
                ", awards=" + awards +
                '}';
    }
}

