package com.prehax.award.models;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @Column(name = "cid")
    private int cid;
    private String name;
    @Column(name = "award_points")
    private int awardPoints;

    @OneToMany
    @JoinColumn(name = "customer_cid", foreignKey = @ForeignKey(name = "cid_FK"))
    private List<Transaction> transactions;

    public Customer() {}

    public Customer(int cid, String name, int awardPoints) {
        this.cid = cid;
        this.name = name;
        this.awardPoints = awardPoints;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAwardPoints() {
        return awardPoints;
    }

    public void setAwardPoints(int awardPoints) {
        this.awardPoints = awardPoints;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "cid=" + cid +
                ", name='" + name + '\'' +
                ", awardPoints=" + awardPoints +
                '}';
    }
}
