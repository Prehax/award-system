package com.prehax.award.services;

import com.prehax.award.models.Award;
import com.prehax.award.models.Customer;
import com.prehax.award.models.CustomerAwards;
import com.prehax.award.models.Transaction;
import com.prehax.award.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class CustomerService {
    @Autowired
    CustomerRepository repository;

    private static final DateFormat format = new SimpleDateFormat("yyyy-MM");

    public List<Customer> getAllCustomers() {
        return repository.findAll();
    }

    public Customer getCustomerById(int id) {
        return repository.findById(id).orElse(null);
    }

    public CustomerAwards getAwardsById(int id) {
        Customer customer = repository.findById(id).orElse(null);
        if (customer == null) { return null; }

        List<Transaction> transactions = customer.getTransactions();

        List<Award> awards = new ArrayList<>();
        Award award = null;
        for (int i=0; i< transactions.size(); i++) {
            Transaction t = transactions.get(i);
            long timestamp = t.getDate().getTime();
            String yearMonth = format.format(new Date(timestamp));

            if (award == null) {
                award = new Award(yearMonth, t.getAward());
            } else if (!award.getYearMonth().equals(yearMonth)) {
                awards.add(award);
                award = new Award(yearMonth, t.getAward());
            } else {
                award.setAward(award.getAward()+t.getAward());
            }

            if (i == transactions.size()-1) {
                awards.add(award);
            }
        }

        return new CustomerAwards(id, customer.getName(), customer.getAwardPoints(), awards);
    }



//    @Autowired
//    TransactionRepository transactionRepository;
    //    This method is only for creating the initial dummy data, now it's useless
//    public void init() {
//        for (int i = 37; i < 40; i++) {
//            List<Transaction> byCustomerId = transactionRepository.findAllByCustomerId(i);
//            int award = 0;
//            for (Transaction t : byCustomerId) {
//                award += t.getAward();
//            }
//            Customer customer = new Customer(i, "DummyCustomer"+i, award);
//            repository.save(customer);
//        }
//    }
}
