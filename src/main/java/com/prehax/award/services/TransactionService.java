package com.prehax.award.services;


import com.prehax.award.repositories.CustomerRepository;
import com.prehax.award.models.*;
import com.prehax.award.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class TransactionService {
    @Autowired
    private TransactionRepository repository;
    @Autowired
    private CustomerRepository customerRepository;

    private static final DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public List<Transaction> getAll() {
        return repository.findAll();
    }

    public List<Transaction> getByMonth(int year, int month) {
        if (year < 1970 || month < 1 || year > 9998 || month > 12) {
            return null;
        }
        int endYear = year;
        int endMonth = month + 1;
        if (endMonth == 13) {
            endMonth = 1;
            endYear ++;
        }
        long start = parseTimestamp(year, month, 1);
        long end = parseTimestamp(endYear, endMonth, 1);

        return repository.findTransactionsByDateBetween(new Timestamp(start), new Timestamp(end));
    }

    public List<Transaction> getByPeriod(int startYear, int startMonth, int startDay,
                                         int endYear, int endMonth, int endDay) {
        if (startYear < 1970 || startMonth < 0 || startDay < 0 ||
        endYear < 1970 || endMonth < 0 || endDay < 0 ||
        startYear > 9998 || startMonth > 12 || startDay > 31 ||
        endYear > 9998 || endMonth > 12 || endDay > 31) { return null; }

        // make sure year [1971 ~ 9998]
        // month [1 ~ 12]
        // day [1 ~ 31]
        long start = parseTimestamp(startYear, startMonth, startDay);
        long end = parseTimestamp(endYear, endMonth, endDay);

        return repository.findTransactionsByDateBetween(new Timestamp(start), new Timestamp(end));
    }

    public Transaction getById(long id) {
        Optional<Transaction> transaction = repository.findById(id);
        return transaction.orElse(null);
    }

    public List<Transaction> getByCustomer(int id) {
        return repository.findAllByCustomerCidEquals(id);
    }

    public Transaction deleteById(long id) {
        Transaction transaction = getById(id);
        if (transaction == null) {
            return null;
        } else {
            // delete the transaction record
            repository.deleteById(id);

            // also minus award points in customer table
            int cusId = transaction.getCustomerCid();
            int award = transaction.getAward();

            Customer customer = customerRepository.getById(cusId);
            customer.setAwardPoints(customer.getAwardPoints() - award);
            customerRepository.save(customer);
        }
        return transaction;
    }

    public Transaction add(SimpleTransaction st) {
        int cusId = st.getCustomerId();
        double amount = st.getAmount();
        int award = amount>100 ? (int) amount*2-150 : amount>50 ? (int) amount-50 : 0;
        long id = System.currentTimeMillis();

        Optional<Customer> optionalCustomer = customerRepository.findById(cusId);
        Customer customer;

        if(optionalCustomer.isPresent()) {
            customer = optionalCustomer.get();
            customer.setAwardPoints(customer.getAwardPoints() + award);
        } else {
            customer = new Customer(cusId, "DummyCustomer"+cusId, award);
        }
        Transaction transaction = new Transaction(id, cusId, amount, award, new Timestamp(id));
        customerRepository.save(customer);
        return repository.save(transaction);
    }

    // when invoking, make sure the year [1971 ~ 9998], month [1 ~ 12], day [1 ~ 31]
    private long parseTimestamp(int year, int month, int day) {
        String y = String.valueOf(year);
        String m = month < 10 ? "0"+month : String.valueOf(month);
        String d = day < 10 ? "0"+day : String.valueOf(day);
        Date date = null;
        try {
            date = format.parse(y+"-"+m+"-"+d +" 00:00:00");
        } catch (Exception e) {
            e.printStackTrace();
            return System.currentTimeMillis();
        }
        return date.getTime();
    }


    //    public void init() {
//        repository.initData();
//    }
}
