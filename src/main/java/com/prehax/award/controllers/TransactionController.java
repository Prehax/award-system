package com.prehax.award.controllers;

import com.prehax.award.models.SimpleTransaction;
import com.prehax.award.models.Transaction;
import com.prehax.award.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Controller
@ResponseBody
@RequestMapping(path = "/transaction")
public class TransactionController {

    @Autowired
    TransactionService service;

    // get all transactions
    @GetMapping(path = "/all")
    public List<Transaction> getAllTransactions() {
        return service.getAll();
    }

    // search by month
    @GetMapping(path = "/getByMonth/{year}/{month}")
    public List<Transaction> getTransactionsByMonth(@PathVariable(name = "year") int year, @PathVariable(name = "month") int month) {
        return service.getByMonth(year, month);
    }

    // search by time period
    @GetMapping(path = "/getByPeriod/start/{startYear}/{startMonth}/{startDay}/end/{endYear}/{endMonth}/{endDay}")
    public List<Transaction> getTransactionsByPeriod(@PathVariable(name = "startYear") int startYear,
                                                     @PathVariable(name = "startMonth") int startMonth,
                                                     @PathVariable(name = "startDay") int startDay,
                                                     @PathVariable(name = "endYear") int endYear,
                                                     @PathVariable(name = "endMonth") int endMonth,
                                                     @PathVariable(name = "endDay") int endDay){
        return service.getByPeriod(startYear, startMonth, startDay, endYear, endMonth, endDay);
    }

    // get by Id
    @GetMapping(path = "/getById/{id}")
    public Transaction getTransactionById(@PathVariable(name = "id") long id) {
        return service.getById(id);
    }

    // get By customer
    @GetMapping(path = "/getByCustomer/{id}")
    public List<Transaction> getTransactionsByCustomer(@PathVariable(name = "id") int id) {
        return service.getByCustomer(id);
    }

    // delete a certain transaction
    @DeleteMapping(path = "/deleteById/{id}")
    public Transaction deleteTransactionById(@PathVariable(name = "id") long id) {
        return service.deleteById(id);
    }

    // add a transaction
    @PostMapping(path = "/add")
    public Transaction addTransaction(@RequestBody SimpleTransaction st) {
        return service.add(st);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handler(){
        return null;
    }


    //    @PostMapping(path = "/init")
//    public void init() {
//        service.init();
//    }
}