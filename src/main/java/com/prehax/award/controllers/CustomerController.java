package com.prehax.award.controllers;

import com.prehax.award.models.Customer;
import com.prehax.award.models.CustomerAwards;
import com.prehax.award.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/customer")
public class CustomerController {

    @Autowired
    CustomerService service;

    @GetMapping(path = "/all")
    public List<Customer> getAllCustomers() {
        return service.getAllCustomers();
    }

    @GetMapping(path = "/getById/{id}")
    public Customer getCustomerById(@PathVariable(name = "id") int id) {
        return service.getCustomerById(id);
    }

    @GetMapping(path = "/getAwardsById/{id}")
    public CustomerAwards getAwardsById(@PathVariable(name = "id") int id) {
        return service.getAwardsById(id);
    }


    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handler(){
        return null;
    }

//    This method is only for creating the initial dummy data, now it's useless
//    @PostMapping(path = "/init")
//    public void init() {
//        service.init();
//    }
}
