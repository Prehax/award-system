package com.prehax.award.repositories;

import com.prehax.award.models.Customer;
import com.prehax.award.models.Transaction;
import com.prehax.award.repositories.TransactionRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
}
