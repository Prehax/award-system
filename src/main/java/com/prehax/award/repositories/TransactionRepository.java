package com.prehax.award.repositories;

import com.prehax.award.models.Customer;
import com.prehax.award.models.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.*;

// This is a dummy data source to simulate the database
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {
    List<Transaction> findAllByCustomerCidEquals(int id);
    List<Transaction> findTransactionsByDateBetween(Timestamp start, Timestamp end);


    // All of the below code is to generate dummy data for initialization, now they are useless.
    // length 90, initial dummy transaction amounts, random number [10, 200)
//    double[] amounts = {
//            99.43, 161.45, 106.96, 130.69, 155.64, 115.04, 54.10, 107.59, 119.41,
//            49.75, 104.41, 173.51, 111.20, 167.45, 32.66, 138.20, 7.22, 1.27, 161.38,
//            164.01, 41.09, 186.43, 131.39, 98.47, 140.62, 152.30, 85.01, 182.82, 137.70,
//            30.43, 32.59, 55.75, 187.54, 22.02, 134.63, 86.64, 157.33, 179.74, 105.56,
//            23.18, 168.85, 192.48, 103.40, 55.59, 98.17, 191.46, 131.75, 188.13, 155.48,
//            73.34, 55.25, 109.97, 129.76, 30.90, 196.45, 119.28, 172.55, 153.97, 92.21,
//            89.70, 92.22, 170.92, 38.20, 29.99, 103.88, 0.98, 40.09, 140.28, 95.22, 26.27,
//            83.65, 52.43, 113.71, 194.89, 136.10, 41.90, 89.61, 110.94, 175.59, 5.51, 17.78,
//            147.86, 88.55, 150.87, 161.50, 8.69, 26.86, 107.95, 24.76, 75.00
//    };
    // length 90, initial dummy customers, random number [0, 40)
//    int[] customerIDs = {
//            23, 0, 16, 32, 2, 4, 30, 23, 18, 32, 19, 25, 22, 0, 30, 14, 30, 32,
//            13, 5, 35, 6, 21, 4, 5, 18, 21, 33, 8, 15, 16, 38, 4, 11, 21, 3, 24,
//            18, 29, 30, 19, 17, 16, 21, 30, 22, 12, 4, 33, 5, 27, 10, 16, 38, 7,
//            22, 34, 34, 5, 7, 23, 3, 28, 33, 3, 35, 32, 11, 2, 29, 7, 5, 18, 24,
//            16, 14, 24, 26, 27, 36, 0, 1, 24, 13, 4, 37, 8, 26, 32, 1
//    };

//    default void initData() {
        // convert dummy data into the data format that we can use
        // amount, customerId is using the arrays like above,
        // month grows as 0, 1, 2, 3 ... 10, 11, 0, 1, 2 ... 10, 11, 0, 1;
//        int award = 0;
//
//        long range = System.currentTimeMillis() - 1599710400000L;
//        Random random = new Random();
//
//        for (int i = 0; i < amounts.length; i++) {
//            award = amounts[i]>100 ? (int) (2*amounts[i])-150 : amounts[i]>50 ? (int) amounts[i]-50 : 0;
//            Timestamp date = new Timestamp((long)(random.nextDouble() * range) + 1599710400000L);
//            Transaction t = new Transaction(date.getTime(), customerIDs[i], amounts[i], award, date);
//            save(t);
//        }
//    }
}
