# How to run

before that, make sure you have already installed git, java

go to URL:
    
    https://bitbucket.org/Prehax/award-system/src

to get the project git url, and use terminal, run

    git clone https://Prehax@bitbucket.org/Prehax/award-system.git

then, you will find there is one more folder named award appears, now run
    
    cd award
    
now we are in the project folder, then run

    mvnw sprint-boot:run

then open browser, there are several apis running here:
    
get all transactions:

    http://localhost:8080/transaction/all
    
search transactions by month:

    http://localhost:8080/transaction/getByMonth/{year}/{month}
    for example:
    http://localhost:8080/transaction/getByMonth/2020/12
    http://localhost:8080/transaction/getByMonth/2021/1

search transactions by a period:
    
    http://localhost:8080/transaction/getByPeriod/start/{startYear}/{startMonth}/{startDay}/end/{endYear}/{endMonth}/{endDay}
    for example:
    http://localhost:8080/transaction/getByPeriod/start/2020/1/3/end/2021/1/3
    because you can choose any period, so this API satisfied the requirement:
    "Given a record of every transaction during a three month period"
    
get transaction by id:
    
    http://localhost:8080/transaction/getById/{id}
    for example:
    http://localhost:8080/transaction/getById/1600355503366

get transaction by customer:

    http://localhost:8080/transaction/getByCustomer/{id}
    for example:
    http://localhost:8080/transaction/getByCustomer/5

delete a transaction by id (need to be operated on postman):

    http://localhost:8080/transaction/deleteById/{id}
    for example:
    http://localhost:8080/transaction/deleteById/1600355503366

add a transaction:
    
    http://localhost:8080/transaction/add
    and you need to provide an SimpleTransaction json object,
    include customerId and amount.
    for example:
```json
{
    "customerId":42,
    "amount":52.2
}
``` 

get all customers:
    
    http://localhost:8080/customer/all

get customer by id:

    http://localhost:8080/customer/getById/{id}

calculate the reward points earned for each customer per month and total:
    
    http://localhost:8080/customer/getAwardsById/{id}

That's all, thank you!
